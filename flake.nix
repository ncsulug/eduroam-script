{
  description = "Application packaged using poetry2nix";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      # see https://github.com/nix-community/poetry2nix/tree/master#api for more functions and examples.
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      packages = {
        script = pkgs.callPackage ./script {};
        default = self.packages.${system}.script;
      };

      # devShells.default = pkgs.mkShell {
      #   inputsFrom = [self.packages.${system}.myapp];
      # };
    });
}
