{
  bash,
  python3,
  openssl,
  simpleTpmPk11,
  which,
  cacert,
  firefox,
  xdg-utils,
  hostname,
  networkmanager,
  writeShellApplication,
  writeTextFile,
  #script tools
  patch,
  curl,
}: let
  buildInputs = [
    bash
    (
      python3.withPackages
      (ps: [
        ps.dbus-python
      ])
    )
    openssl
    simpleTpmPk11
    which
    cacert
    bash
    firefox
    xdg-utils
    hostname
    networkmanager
    patch
    curl
  ];

  JoinNowPatch = writeTextFile {
    name = "JoinNow.patch";
    text = builtins.readFile ./JoinNow.patch;
  };

  pythonPatch = writeTextFile {
    name = "python.patch";
    text = builtins.readFile ./python.patch;
  };
in
  writeShellApplication {
    name = "eduroam-installer";

    runtimeInputs = buildInputs;

    text = ''

      cleanup() {
          printf "\nCleaning Up"
          cd ../../
          rm -r tmp
          rm ./SecureW2_JoinNow.run.orig ./SecureW2_JoinNow.run
          exit
      }

      curl 'https://cloud.securew2.com/public/12117/eduroam/php/deploy.php' -X POST --data-raw 'request=deployLinux' -o SecureW2_JoinNow.run
      patch ./SecureW2_JoinNow.run ${JoinNowPatch} -b
      bash ./SecureW2_JoinNow.run
      cd tmp/*
      patch detect.py ${pythonPatch}
      trap cleanup SIGINT
      python3 main.py
      cleanup
    '';
  }
# stdenv.mkDerivation {
#   pname = "eduroam-install-script";
#   version = "0.2";
#
#   src = ./.;
#
#   inherit buildInputs;
#
#
#   nativeBuildInputs = [makeWrapper];
#   installPhase = ''
#     mkdir -p $out/bin
#     cp eduroam-install/bin/eduroam-install $out/bin/script
#     cp ./JoinNow.patch $out/lib/JoinNow.patch
#     cp ./python.patch $out/lib/python.patch
#     wrapProgram $out/bin/github-downloader.sh \
#       --prefix PATH : ${lib.makeBinPath buildInputs}
#   '';
# }

