# Eduroam Script

 This is a script to get eduroam working out of the box on various Linux devices. While it was initially made for personal use with [nixOS](https://nixos.org/), it can be used on any Linux system.

## Dependencies
The script makes use of the [nix package manager](https://nixos.org/download.html) to provide dependencies. Follow the installation instructions on the official website. Additionally, the target system must use Network-Manager (networkmanager).

## Instructions
As of version 0.2, this project can now be run with `nix run 'gitlab:ncsulug/eduroam-script'`. This requires flakes. If you don't have flakes enabled, running with `nix run 'gitlab:ncsulug/eduroam-script' --experimental-features 'nix-command flakes'` will solve the issue
